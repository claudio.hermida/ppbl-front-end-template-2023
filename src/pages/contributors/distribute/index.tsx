import { Box, Button, Center, Heading, Spinner, Text } from "@chakra-ui/react";
import { CardanoWallet, useAddress, useWallet } from "@meshsdk/react";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useContext, useEffect, useState } from "react";
import {
  escrowAddress,
  escrowReferenceUTxO,
  projectAsset,
  contributorPolicyID,
  issuerPolicyID,
  metadataKey,
  issuerAsset,
} from "gpte-config";
import { gql, useQuery } from "@apollo/client";
import { CONTRIBUTOR_TOKEN_QUERY } from "@/src/data/queries/contributorQueries";
import { Asset, Transaction, UTxO } from "@meshsdk/core";
import { GraphQLToDatum, hexToString } from "@/src/utils";
import { GraphQLInputUTxO, GraphQLToken, GraphQLUTxO } from "@/src/types/cardanoGraphQL";
import DistributeTx from "@/src/components/gpte/transactions/DistributeTx";
import { ProjectTxMetadata } from "@/src/types/project";
import GPTENav from "@/src/components/gpte/GPTENav";
import { PPBLContext } from "@/src/context/PPBLContext";

const ESCROW_QUERY = gql`
  query UtxosAtEscrowAddress($address: String!) {
    utxos(where: { address: { _eq: $address } }) {
      txHash
    }
  }
`;

export default function DistributePage(txHash: string) {
  const { connected, wallet } = useWallet();
  const connectedAddress = useAddress();
  const [assets, setAssets] = useState<null | any>(null);
  const [isLoading, setLoading] = useState<boolean>(false);

  const ppblContext = useContext(PPBLContext);

  const { data, loading, error } = useQuery(ESCROW_QUERY, {
    variables: {
      address: escrowAddress,
    },
  });

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  return (
    <>
      <Head>
        <title>PPBL 2023</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <GPTENav />
      <Box bgImage="url(/bg2.jpeg)" no-repeat center-fixed backgroundSize="cover" width="100%" minHeight="100vh" p="5">
        <Box w="80%" mx="auto" zIndex={15} p="5" bg="theme.dark">
          <Heading>Distribute Commitments</Heading>
          <Text>{connectedAddress}</Text>
          <Heading size="md">To Do:</Heading>
          <Text py="1">Create different instances of DistributeTx with different Contributor Reference Datum</Text>
          {ppblContext.connectedIssuerToken ? (
            <Box>
              <Text>You have a PPBL2023Teacher Token</Text>
            </Box>
          ) : (
            <Box>
              <Text>A PPBL2023Teacher token is required to unlock commitments.</Text>
            </Box>
          )}
          <Box>
            {data.utxos.map((utxo: any) => (
              <Box key={utxo.txHash}>
                <DistributeTx txHash={utxo.txHash} />
              </Box>
            ))}
          </Box>
        </Box>
      </Box>
    </>
  );
}
