{
  "lessons": [
    {
      "slug": "2031",
      "introduction": [
        "On Cardano, metadata can be added to any transaction, for any purpose.",
        "Right now, one of the most popular use cases of metadata is for minting Cardano NFTs. In this lesson, we will take a quick look at how to include simple metadata in a transaction. Then, in Lesson 203.2, you will see how to use metadata to mint an NFT.",
        "Transaction metadata cannot be used directly in smart contracts, but it can be used to include relevant information about a transaction, or simply to store some information permanently on the blockchain (which is how NFTs work).",
        "Metadata can be added to a transaction using several of the tools we have investigated already: Cardano CLI, MeshJS, or GameChanger Wallet, for example. For now, we will focus on Cardano CLI, but we encourage you to continue exploring with whatever tools interest you most!"
      ],
      "links": [
        {
          "url": "https://developers.cardano.org/docs/transaction-metadata/",
          "linkText": "Cardano Developer Portal: Build with Transaction Metadata"
        },
        {
          "url": "https://github.com/input-output-hk/cardano-node/blob/master/doc/reference/tx-metadata.md",
          "linkText": "Cardano Node docs: Transaction Metadata"
        },
        {
          "url": "https://meshjs.dev/apis/transaction#setMetadata",
          "linkText": "MeshJS: setMetadata()"
        }
      ],
      "videoHeading": "Introduction to Transaction Metadata",
      "youtubeId": "frTPcSyfSEE",
      "successComponent": false,
      "success": {
        "criteria": "",
        "text": ""
      },
      "assignmentComponent": true,
      "markdownComponent": false
    },
    {
      "slug": "2032",
      "introduction": [
        "The Cardano NFT ecosystem is built on a set of agreements.",
        "First, anyone who mints an NFT agrees to follow a certain set of standards for how transaction metadata is used to represent a media file.",
        "Then, developers of wallets, dapps, and web sites agree to create tools that display the media specified in NFTs.",
        "A robust NFT ecosystem exists on Cardano because a global network of people have agreed to use the same standard.",
        "This means that \"Cardano NFTs\" can evolve: people can agree to start new experiments and to create new standards, depending on use cases. That's exactly what is happening in the Cardano ecosystem, right now."
      ],
      "links": [
        {
          "url": "https://cips.cardano.org/cips/cip25/",
          "linkText": "CIP 25 - Media NFT Metadata Standard"
        }
      ],
      "videoHeading": "How to mint a CIP-25 NFT with Cardano CLI",
      "youtubeId": "pO18jrw7GkA",
      "successComponent": false,
      "success": {
        "criteria": "",
        "text": ""
      },
      "assignmentComponent": true,
      "markdownComponent": false
    },
    {
      "slug": "2033",
      "introduction": [
        "Cardano is designed to evolve over time.",
        "Cardano Improvement Proposals (CIPs) provide that anyone can use to contribute to the evolution of Cardano.",
        "Even if you are not yet ready to write a CIP on your own, a really good way to learn about Cardano development is to read existing CIPs."
      ],
      "links": [
        {
          "url": "https://cips.cardano.org/",
          "linkText": "Cardano Improvement Proposals"
        }
      ],
      "videoHeading": "Bookmark this page:",
      "youtubeId": "5pV32jaQGSw",
      "successComponent": false,
      "success": {
        "criteria": "",
        "text": ""
      },
      "assignmentComponent": true,
      "markdownComponent": false
    },
    {
      "slug": "2034",
      "introduction": [
        "The PPBL 2023 Contributor Token, minted in Lesson 100.4, is one of the projects that we will continue to revisit throughout this course. The token implementation makes partial use of CIP-68.",
        "What is exciting about new standards is that we can experiment with them. By doing so, we can figure out works, and we can identify ways that a CIP can improved. Sometimes, we might even discover that a new CIP can be proposed.",
        "Right now the PPBL 2023 token datum contains your lucky number and a list of completed modules. At this stage in our experiment, are you happy with this data? How do you think it can be changed? What would you like to be able to do with your PPBL 2023 Contributor Token?"
      ],
      "links": [
        {
          "url": "https://cips.cardano.org/cips/cip68/",
          "linkText": "CIP 68 - Datum Metadata Standard"
        }
      ],
      "videoHeading": "PPBL 2023 Token + CIP-68",
      "youtubeId": "es91dSO5I8Q",
      "successComponent": false,
      "success": {
        "criteria": "",
        "text": ""
      },
      "assignmentComponent": true,
      "markdownComponent": false
    }
  ]
}
