import ProjectLayout from "@/src/components/lms/Lesson/ProjectLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module204.json";
import DocsStep02 from "@/src/components/course-modules/204/DocsStep02.mdx";

export default function ProjectStep02() {
  const slug = "project-step-02";
  const title = "Step 2: Deploy a Reference Script"

  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <ProjectLayout moduleNumber={204} title={title} sltId="102.3" slug={slug}>
      <DocsStep02 />
    </ProjectLayout>
  );
}
